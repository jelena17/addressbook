﻿using App;
using App.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Hubs;

namespace WebApi.Controllers
{
    [ApiController]
    public class ContactsController
        : ControllerBase
    {
        private readonly ICommand<CreateContactRequest> _createContact;
        private readonly ICommand<int> _deleteContact;
        private readonly ICommand<UpdateContactRequest> _updateCommand;
        private readonly IQuery<int, GetContactResponse> _getContact;
        private readonly IQuery<GetContactsRequest, IEnumerable<GetContactResponse>> _getContacts;
        private readonly IHubContext<ContactHub> _contactHub;

        public ContactsController(
            ICommand<CreateContactRequest> createContact,
            ICommand<int> deleteContact,
            IQuery<int, GetContactResponse> getContact,
            IQuery<GetContactsRequest, IEnumerable<GetContactResponse>> getContacts,
            ICommand<UpdateContactRequest> updateCommand,
            IHubContext<ContactHub> contactHub)
        {
            _createContact = createContact;
            _deleteContact = deleteContact;
            _getContact = getContact;
            _getContacts = getContacts;
            _updateCommand = updateCommand;
            _contactHub = contactHub;
        }

        [HttpGet]
        [Route("/contacts/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var contact = await _getContact.Execute(id);
            return Ok(contact);
        }

        [HttpGet]
        [Route("/contacts")]
        public async Task<IActionResult> Get(
            [FromQuery] GetContactsRequest request)
        {
            var contacts = await _getContacts.Execute(request);
            return Ok(contacts);
        }

        [HttpPost]
        [Route("/contacts")]
        public async Task<IActionResult> Create([FromBody] CreateContactRequest request)
        {
            await _createContact.Execute(request);
            return Created("", null);
        }

        [HttpPut]
        [Route("/contacts/{id}")]
        public async Task<IActionResult> Update(
            [FromRoute] int id,
            [FromBody] UpdateContactRequest request)
        {
            request.Id = id;

            await _updateCommand.Execute(request);
            await _contactHub.Clients.All.SendAsync("ContactUpdate", request);
            return Ok();
        }

        [HttpDelete]
        [Route("/contacts/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _deleteContact.Execute(id);
            return Ok();
        }
    }
}