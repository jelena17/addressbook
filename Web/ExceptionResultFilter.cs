﻿using DataAccess.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace Web
{
    public class ExceptionResultFilter :
       IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            context.Result = CreateResult(context.Exception);
        }

        private IActionResult CreateResult(Exception exception)
        {
            if (exception is EntityNotFoundException)
                return new NotFoundObjectResult(exception.Message);

            if (exception is EntityAlreadyExistsException)
                return new ConflictObjectResult(exception.Message);

            if (exception is ArgumentException)
                return new BadRequestObjectResult(new { exception.Message });

            return new ObjectResult(exception.Message) { StatusCode = 500 };
        }
    }
}