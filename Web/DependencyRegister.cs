﻿using App;
using App.Commands;
using App.Contracts;
using App.Queries;
using App.Repositories;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Web
{
    public static class DependencyRegister
    {
        public static void Register(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IContactCommandRepository, ContactCommandRepository>();
            services.AddScoped<IContactQueryRepository, ContactQueryRepository>();
            services.AddScoped<ICommand<CreateContactRequest>, CreateContactCommand>();
            services.AddScoped<ICommand<int>, DeleteContactCommand>();
            services.AddScoped<ICommand<UpdateContactRequest>, UpdateContactCommand>();
            services.AddScoped<IQuery<int, GetContactResponse>, GetContactQuery>();
            services.AddScoped<IQuery<GetContactsRequest, IEnumerable<GetContactResponse>>, GetContactsQuery>();

            services.AddDbContext<ContactDbContext>(optionsBuilder =>
               optionsBuilder.UseNpgsql(configuration["ConnectionStrings:ContactDb"])
            );
        }
    }
}