﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class FullNameTests
    {
        [TestMethod]
        [DataRow("Marko Markovic")]
        [DataRow("Marko Marković")]
        [DataRow("Marko A. Marković")]
        [DataRow("Dr Marko Marković")]
        [DataRow("Jack IV Farley")]
        [DataRow("Jack O`Nely")]
        [DataRow("Test Test")]
        public void Able_to_create_fullName(string value)
        {
            var actual = new FullName(value);
            actual.Value.Should().BeEquivalentTo(value);
        }

        [TestMethod]
        [DataRow("")]
        [DataRow("  ")]
        [DataRow("Marko 1")]
        [DataRow("Marko 1 Markovic")]
        [DataRow("MarkoMarkovic")]
        [DataRow("00000")]
        [DataRow(null)]
        public void Throw_error_on_invalid_fullName(string value)
        {
            Action action = () => _ = new FullName(value);

            action
                .Should()
                .Throw<ArgumentException>()
                .WithMessage("Invalid value for FullName.");
        }
    }
}