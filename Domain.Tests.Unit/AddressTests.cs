﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class AddressTests
    {
        [TestMethod]
        public void Able_to_create_address()
        {
            var actual = new Address("Ul. Dalmatinska 156", "Podgorica", "81156", "CG");

            actual.AddresLine
                .Should().BeEquivalentTo("Ul. Dalmatinska 156");
            actual.City
                .Should().BeEquivalentTo("Podgorica");
            actual.Zip.Value
                .Should().BeEquivalentTo("81156");
            actual.Country.Value
                .Should().BeEquivalentTo("CG");
        }

        [TestMethod]
        [DataRow(null, "Podgorica", "81156", "CG", "Invalid value for address line.")]
        [DataRow("", "Podgorica", "81156", "CG", "Invalid value for address line.")]
        [DataRow("Ul. Dalmatinska", "Podgorica", "81156", "", "Invalid value for Country.")]
        [DataRow("Ul. Dalmatinska", "Podgorica", "", "Cg", "Invalid value for Zip.")]
        [DataRow("Ul. Dalmatinska", null, "81156", "Cg", "Invalid value for city.")]
        public void Throw_error_on_invalid_address(
            string addersLine, string city, string zip, string country, string expectedMessage)
        {
            Action action = () => _ = new Address(addersLine, city, zip, country);

            action
                .Should()
                .Throw<ArgumentException>()
                .WithMessage(expectedMessage);
        }
    }
}