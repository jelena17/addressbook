﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class DateOfBirthTests
    {
        [TestMethod]
        public void Able_to_create_dateOfBirht()
        {
            var actual = new DateOfBirth(new DateTime(2000, 1, 1));

            actual.Should().NotBeNull();
        }

        public void Throw_error_on_invalid_dateOfBirth()
        {
            Action action = () => _ = new DateOfBirth(DateTime.Now.AddDays(5));

            action
                .Should()
                .Throw<ArgumentException>()
                .WithMessage("");
        }
    }
}