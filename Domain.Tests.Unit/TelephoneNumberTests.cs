﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class TelephoneNumberTests
    {
        [TestMethod]
        [DataRow("+31 621 561 100")]
        [DataRow("+31621561100")]
        [DataRow("+38267 111-111")]
        [DataRow("067 111 111")]
        [DataRow("(067) 111 111")]
        [DataRow("067111111")]
        [DataRow("067-111-111")]
        public void Able_to_create_telephone_number(string value)
        {
            var actual = new TelephoneNumber(value);

            actual.Value
                .Should().BeEquivalentTo(value);
        }

        [TestMethod]
        [DataRow("")]
        [DataRow("067111111Ab")]
        [DataRow("0671111119090909090999")]
        [DataRow("---067111111")]
        [DataRow("067--111--111")]
        [DataRow("000")]
        [DataRow(null)]
        public void Throw_error_on_invalid_telephoneNumber(string value)
        {
            Action action = () => _ = new TelephoneNumber(value);

            action
                .Should()
                .Throw<ArgumentException>()
                .WithMessage("Invalid value for TelephoneNumber.");
        }
    }
}