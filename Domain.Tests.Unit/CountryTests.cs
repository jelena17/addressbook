﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class CountryTests
    {
        [TestMethod]
        [DataRow("HR")]
        [DataRow("CG")]
        [DataRow("UK")]
        [DataRow("US")]
        [DataRow("CN")]
        [DataRow("us")]
        [DataRow("Us")]
        public void Able_to_create_country(string value)
        {
            var actual = new Country(value);

            actual.Value
                .Should().BeEquivalentTo(value);
        }

        [TestMethod]
        [DataRow("")]
        [DataRow("  ")]
        [DataRow("CHN")]
        [DataRow("CHINA")]
        [DataRow(null)]
        public void Throw_error_on_invalid_country(string value)
        {
            Action action = () => _ = new Country(value);

            action
                .Should()
                .Throw<ArgumentException>()
                .WithMessage("Invalid value for Country.");
        }
    }
}