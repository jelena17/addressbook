﻿namespace Domain.Tests.Unit
{
    public record FakeRecord : ValueObject<FakeRecord>
    {
        public int Property1 { get; }
        public bool Property2 { get; }
        public string Property3 { get; }

        public FakeRecord(int property1, bool property2, string property3)
        {
            Property1 = property1;
            Property2 = property2;
            Property3 = property3;
        }
    }
}