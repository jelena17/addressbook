﻿namespace Domain.Tests.Unit
{
    public record FakeComplexRecord : ValueObject<FakeRecord>
    {
        public FakeRecord Property1 { get; }

        public FakeComplexRecord(FakeRecord property1)
        {
            Property1 = property1;
        }
    }
}