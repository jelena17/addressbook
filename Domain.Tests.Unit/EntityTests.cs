﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class EntityTests
    {
        [TestMethod]
        public void Able_to_compare_two_same_entities()
        {
            var first = new FakeEntity(1);
            var second = new FakeEntity(1);

            var isEqual = first.Equals(second);

            isEqual
                .Should().BeTrue();

            first
                .Should().BeEquivalentTo(second);
        }

        [TestMethod]
        public void Able_to_compare_two_different_entities()
        {
            var first = new FakeEntity(1);
            var second = new FakeEntity(2);

            var isEqual = first.Equals(second);

            isEqual
                .Should().BeFalse();

            first
                .Should().NotBeEquivalentTo(second);
        }
    }
}