﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class ZipTests
    {
        [TestMethod]
        [DataRow("95321")]
        [DataRow("95321-4321")]
        [DataRow("95321 4321")]
        [DataRow("953214321")]
        [DataRow("81000")]
        [DataRow("10000")]
        [DataRow("518000")]
        public void Able_to_create_zip(string value)
        {
            var actual = new Zip(value);

            actual.Value.Should().Be(value);
        }

        [TestMethod]
        [DataRow("")]
        [DataRow("  ")]
        [DataRow("    5")]
        [DataRow("5    ")]
        [DataRow("123-4444-555")]
        [DataRow("1231231234123")]
        [DataRow(null)]
        public void Throw_error_on_invalid_zip(string value)
        {
            Action action = () => _ = new Zip(value);

            action
                .Should()
                .Throw<ArgumentException>()
                .WithMessage("Invalid value for Zip.");
        }
    }
}