using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Domain.Tests.Unit
{
    [TestClass]
    public class ValueObjectTests
    {
        [TestMethod]
        [DataRow(1, false, "Test")]
        [DataRow(2, true, null)]
        [DataRow(3, true, "")]
        public void Able_to_create_simple_value_object(int prop1, bool prop2, string prop3)
        {
            var actual = new FakeRecord(prop1, prop2, prop3);

            actual.Should().NotBeNull();
            actual.Property1.Should().Be(prop1);
            actual.Property2.Should().Be(prop2);
            actual.Property3.Should().Be(prop3);
        }

        [TestMethod]
        [DataRow(1, false, "Test")]
        [DataRow(2, true, null)]
        [DataRow(3, true, "")]
        public void Able_to_create_nested_value_object(int prop1, bool prop2, string prop3)
        {
            var actualSimpleObject = new FakeRecord(prop1, prop2, prop3);
            var actualComplexObject = new FakeComplexRecord(actualSimpleObject);

            actualComplexObject.Should().NotBeNull();
            actualComplexObject.Property1.Property1.Should().Be(prop1);
            actualComplexObject.Property1.Property2.Should().Be(prop2);
            actualComplexObject.Property1.Property3.Should().Be(prop3);
        }

        [TestMethod]
        public void Able_to_compare_simple_value_object_that_are_same()
        {
            var first = new FakeRecord(1, true, "Test");
            var second = new FakeRecord(1, true, "Test");

            var isEqual = first.Equals(second);

            isEqual.Should().BeTrue();
        }

        [TestMethod]
        public void Able_to_compare_simple_value_object_that_are_not_same()
        {
            var first = new FakeRecord(1, true, "Test");
            var second = new FakeRecord(1, true, "Test1");

            var isEqual = first.Equals(second);

            isEqual.Should().BeFalse();
        }

        [TestMethod]
        public void Able_to_compare_complex_value_object_that_are_same()
        {
            var firstSimple = new FakeRecord(1, true, "Test");
            var firstComplex = new FakeComplexRecord(firstSimple);

            var secondSimple = new FakeRecord(1, true, "Test");
            var secondComplex = new FakeComplexRecord(secondSimple);

            var isEqual = firstComplex.Equals(secondComplex);
            isEqual.Should().BeTrue();
            firstComplex.Should().BeEquivalentTo(secondComplex);
        }

        [TestMethod]
        public void Able_to_compare_complex_value_object_that_are_not_same()
        {
            var firstSimple = new FakeRecord(1, true, "Test");
            var firstComplex = new FakeComplexRecord(firstSimple);

            var secondSimple = new FakeRecord(2, true, "Test");
            var secondComplex = new FakeComplexRecord(secondSimple);

            var isEqual = firstComplex.Equals(secondComplex);
            isEqual.Should().BeFalse();
            firstComplex.Should().NotBeEquivalentTo(secondComplex);
        }
    }
}