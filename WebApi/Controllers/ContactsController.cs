﻿using App;
using App.Contracts;
using Domain;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [ApiController]
    public class ContactsController
        : ControllerBase
    {
        private readonly ICommand<CreateContactRequest> _createContact;
        private readonly ICommand<int> _deleteContact;
        private readonly IQuery<int, Contact> _getContact;
        private readonly IQuery<GetContactsRequest, IEnumerable<Contact>> _getContacts;

        public ContactsController(
            ICommand<CreateContactRequest> createContact,
            ICommand<int> deleteContact,
            IQuery<int, Contact> getContact,
            IQuery<GetContactsRequest, IEnumerable<Contact>> getContacts)
        {
            _createContact = createContact;
            _deleteContact = deleteContact;
            _getContact = getContact;
            _getContacts = getContacts;
        }

        [HttpGet]
        [Route("/contacts/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var contact = await _getContact.Execute(id);
            return Ok(contact);
        }

        [HttpGet]
        [Route("/contacts")]
        public async Task<IActionResult> Get(
            [FromQuery] GetContactsRequest request)
        //[FromQuery] int pageSize = 20, [FromQuery] int pageNumber = 1)
        {
            // var request = new GetContactsRequest { PageNumber = pageNumber, PageSize = pageSize };
            var contacts = await _getContacts.Execute(request);
            return Ok(contacts);
        }

        [HttpPost]
        [Route("/contacts")]
        public async Task<IActionResult> Create([FromBody] CreateContactRequest request)
        {
            await _createContact.Execute(request);
            return Created("", null);
        }

        [HttpDelete]
        [Route("/contacts/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _deleteContact.Execute(id);
            return Ok();
        }
    }
}