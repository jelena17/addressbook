using App;
using App.Commands;
using App.Contracts;
using App.Queries;
using App.Repositories;
using DataAccess;
using Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace WebApi
{
    public static class DependencyRegister
    {
        public static void Register(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IContactCommandRepository, ContactCommandRepository>();
            services.AddScoped<IContactQueryRepository, ContactQueryRepository>();
            services.AddScoped<ICommand<CreateContactRequest>, CreateContactCommand>();
            services.AddScoped<ICommand<int>, DeleteContactCommand>();
            services.AddScoped<IQuery<int, Contact>, GetContactQuery>();
            services.AddScoped<IQuery<GetContactsRequest, IEnumerable<Contact>>, GetContactsQuery>();

            services.AddDbContext<ContactDbContext>(optionsBuilder =>
               optionsBuilder.UseNpgsql(configuration["ConnectionStrings:ContactDb"])
            );
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            DependencyRegister.Register(services, Configuration);
            services.AddSwaggerGen(c =>
               {
                   c.SwaggerDoc("v1", new OpenApiInfo { Title = "Web", Version = "v1" });
               });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}