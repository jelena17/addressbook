using App.Repositories;
using DataAccess.Exceptions;
using Domain;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace DataAccess.Tests.Integration
{
    [TestClass]
    public class ContactRepositoryTest
        : DbIntegrationTest
    {
        private IContactCommandRepository _commandRepo;
        private IContactQueryRepository _queryRepo;

        [TestInitialize]
        public void Initialzie()
        {
            _commandRepo = new ContactCommandRepository(_context);
            _queryRepo = new ContactQueryRepository(_context);
        }

        [TestMethod]
        public async Task Able_to_create_contact_in_db()
        {
            Contact newContact = BuildData();
            await _commandRepo.Save(newContact);

            Contact actualSaved = await
                _queryRepo.GetById(newContact.Id);

            actualSaved
                .Should()
                .BeEquivalentTo(newContact);
        }

        [TestMethod]
        public async Task Able_to_update_contact_in_db()
        {
            Contact existingContact = await CreateContactInDb();

            var name = new FullName("Update Name");
            existingContact.UpdateName(name);

            await _commandRepo.Save(existingContact);

            Contact actualSaved = await _queryRepo.GetById(existingContact.Id);

            actualSaved
              .Should()
              .BeEquivalentTo(existingContact);
        }

        [TestMethod]
        public async Task Able_to_delete_contact_in_db()
        {
            Contact newContact = await CreateContactInDb();

            await _commandRepo.Delete(newContact.Id);

            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(() =>
                _queryRepo.GetById(newContact.Id));
        }

        [TestMethod]
        public async Task Able_to_get_contact_from_db()
        {
            Contact expected = await CreateContactInDb();
            Contact actual = await _queryRepo.GetById(expected.Id);

            actual
              .Should()
              .BeEquivalentTo(expected);
        }

        private async Task<Contact> CreateContactInDb()
        {
            Contact newContact = BuildData();
            await _commandRepo.Save(newContact);
            return newContact;
        }

        private static Contact BuildData()
        {
            var name = new FullName("Marko Markovic Test");
            var dateOfBirth = new DateOfBirth(2000, 1, 1);
            var address = new Address("Test", "Test", "12345", "US");
            var telephoneNums = new[] {
                new TelephoneNumber("067 000 000")
            };

            return new Contact(name, dateOfBirth, address, telephoneNums);
        }
    }
}