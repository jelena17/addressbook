﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataAccess.Tests.Integration
{
    public class DbIntegrationTest
    {
        protected IDbContextTransaction _transaction;
        protected ContactDbContext _context;

        [TestInitialize]
        public virtual void Initialize()
        {
            SetUpContext();
            _transaction = _context.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _transaction.Rollback();
        }

        private void SetUpContext()
        {
            var services = new ServiceCollection();
            services.AddDbContext<ContactDbContext>(p =>
            {
                p.UseNpgsql("Server=localhost;Port=5432;Database=InfinumDb;User ID=postgres;Password=postgres;");
            });
            var provider = services.BuildServiceProvider();
            _context = provider.GetService<ContactDbContext>();
        }
    }
}