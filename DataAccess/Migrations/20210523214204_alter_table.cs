﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class alter_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Address Line",
                table: "Contacts",
                newName: "AddressLine");

            migrationBuilder.AlterColumn<string>(
                name: "TelephoneNumber",
                table: "TelephoneNumber",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
            migrationBuilder.Sql("ALTER TABLE \"Contacts\" ADD CONSTRAINT unique_addre_and_name UNIQUE(\"FullName\",\"AddressLine\",\"Zip\",\"Country\"); ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AddressLine",
                table: "Contacts",
                newName: "Address Line");

            migrationBuilder.AlterColumn<string>(
                name: "TelephoneNumber",
                table: "TelephoneNumber",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");
        }
    }
}