﻿using App.Repositories;
using DataAccess.Exceptions;
using Domain;
using System;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ContactCommandRepository
        : IContactCommandRepository
    {
        private readonly ContactDbContext _db;
        private const string duplicate_contact_exception_message = "duplicate key value violates unique constraint \"unique_addre_and_name\"";

        public ContactCommandRepository(ContactDbContext db)
        {
            _db = db;
        }

        public async Task Save(Contact contact)
        {
            try
            {
                if (contact.Id == default)
                    _db.Add(contact);
                else
                    _db.Update(contact);

                await _db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (e.InnerException.Message.Contains(duplicate_contact_exception_message))
                    throw new EntityAlreadyExistsException("Contact with same name and address already exists");
            }
        }

        public async Task Delete(int contactId)
        {
            var contact = await _db.Contacts
                .FindAsync(contactId);

            if (contact == default)
                throw new EntityNotFoundException($"Resource with id {contactId} not found.");

            _db.Contacts.Remove(contact);
            await _db.SaveChangesAsync();
        }
    }
}