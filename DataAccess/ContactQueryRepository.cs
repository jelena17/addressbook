﻿using App.Repositories;
using DataAccess.Exceptions;
using Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ContactQueryRepository
        : IContactQueryRepository
    {
        private readonly ContactDbContext _db;

        public ContactQueryRepository(ContactDbContext db)
        {
            _db = db;
        }

        public async Task<Contact> GetById(int contactId)
        {
            var contact = await _db.Contacts.FindAsync(contactId);

            if (contact is null)
                throw new EntityNotFoundException($"Resource with {contactId} not found");

            return contact;
        }

        public async Task<IEnumerable<Contact>> Get(int pageSize, int pageNumber)
        {
            return await _db.Contacts
                .OrderBy(x => x.FullName.Value)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
    }
}