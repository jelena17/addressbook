﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class ContactDbContext : DbContext
    {
        public ContactDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>(contact =>
                {
                    contact.HasKey(c => c.Id);
                    contact.ToTable("Contacts");

                    contact.OwnsOne(c => c.FullName)
                        .Property(p => p.Value)
                        .HasMaxLength(50)
                        .HasColumnName("FullName");

                    contact.OwnsOne(c => c.DateOfBirth)
                        .Property(p => p.Value)
                        .HasColumnName("DateOfBirth");

                    contact.OwnsOne(c => c.Address, addressConfig =>
                    {
                        addressConfig.Property(p => p.AddresLine)
                            .HasMaxLength(100)
                            .HasColumnName("AddressLine");

                        addressConfig.Property(p => p.City)
                           .HasMaxLength(50)
                           .HasColumnName("City")
                           .IsRequired();

                        addressConfig.Property(p => p.State)
                           .HasMaxLength(50)
                           .HasColumnName("State");

                        addressConfig.OwnsOne(address => address.Zip, zipConfig =>
                         {
                             zipConfig.Property(p => p.Value)
                                .HasMaxLength(15)
                                .HasColumnName("Zip");
                         });

                        addressConfig.OwnsOne(address => address.Country, countryConfig =>
                        {
                            countryConfig
                            .Property(p => p.Value)
                            .HasMaxLength(2)
                            .HasColumnName("Country");
                        });
                    });

                    contact.OwnsMany(c => c.TelephoneNumbers, numConfig =>
                    {
                        numConfig.Property(p => p.Value)
                           .HasColumnName("TelephoneNumber")
                           .IsRequired();
                        numConfig.HasKey("ContactId", "Id");
                    });
                });
        }
    }
}