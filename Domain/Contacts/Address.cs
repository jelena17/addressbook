﻿using System;

namespace Domain
{
    public record Address : ValueObject<Address>
    {
        public string AddresLine { get; }
        public string City { get; }
        public string State { get; }
        public Zip Zip { get; }
        public Country Country { get; }

        public Address(string addressLine, string city, string state, string zip, string country)
        {
            ValidateIsNotEmpty(addressLine, "Invalid value for address line.");
            ValidateIsNotEmpty(city, "Invalid value for city.");

            AddresLine = addressLine;
            City = city;
            State = state;
            Zip = new Zip(zip);
            Country = new Country(country);
        }

        public Address(string addressLine, string city, string zip, string country)
         : this(addressLine, city, null, zip, country) { }

        private Address()
        {
        }
        private void ValidateIsNotEmpty(string value, string message)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                throw new ArgumentException(message);
        }
    }
}