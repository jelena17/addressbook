﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Domain
{
    public record FullName
        : ValueObject<FullName>
    {
        private Regex _pattern = new Regex(@"^[^0-9]*$");

        public string Value { get; init; }

        public FullName(string value)
        {
            Validate(value);
            Value = value;
        }

        private void Validate(string name)
        {
            if (IsNameNullOrEmpty(name) || !_pattern.IsMatch(name) || IsNotFullName(name))
                throw new ArgumentException($"Invalid value for {nameof(FullName)}.");
        }

        private static bool IsNotFullName(string name)
            => name?.Trim().Split(" ").Count() == 1;

        private static bool IsNameNullOrEmpty(string name)
            => string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name);
    }
}