﻿using System;
using System.Text.RegularExpressions;

namespace Domain
{
    public record Country : ValueObject<Country>
    {
        private Regex _pattern = new Regex(@"^[\w]{2}$");
        public string Value { get; set; }
        public Country(string value)
        {
            Validate(value);
            Value = value.ToUpper();
        }
        private void Validate(string value)
        {
            if (!_pattern.IsMatch(value ?? ""))
                throw new ArgumentException($"Invalid value for {nameof(Country)}.");
        }
    }
}