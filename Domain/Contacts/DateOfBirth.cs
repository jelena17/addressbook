﻿using System;

namespace Domain
{
    public record DateOfBirth
        : ValueObject<DateOfBirth>
    {
        public DateTime Value { get; init; }
        public DateOfBirth(DateTime value)
        {
            Validate(value);
            Value = value;
        }
        public DateOfBirth(short year, short month, short day)
        {
            var date = new DateTime(year, month, day);
            Validate(date);
            Value = date;
        }

        private void Validate(DateTime value)
        {
            if (value > DateTime.UtcNow)
                throw new ArgumentException($"Invalid value for {nameof(DateOfBirth)}.");
        }
    }
}