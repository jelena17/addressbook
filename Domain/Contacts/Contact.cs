﻿using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Contact : Entity
    {
        public FullName FullName { get; private set; }
        public DateOfBirth DateOfBirth { get; private set; }
        public Address Address { get; private set; }
        private List<TelephoneNumber> _telephoneNumbers;

        public IReadOnlyList<TelephoneNumber> TelephoneNumbers
          => _telephoneNumbers.AsReadOnly();

        public Contact(
            FullName fullName,
            DateOfBirth dateOfBirth,
            Address address,
            IEnumerable<TelephoneNumber> telephoneNumbers)
            : this(0, fullName, dateOfBirth, address, telephoneNumbers) { }

        public Contact(
            int id,
            FullName fullName,
            DateOfBirth dateOfBirth,
            Address address,
            IEnumerable<TelephoneNumber> telephoneNumbers)
            : base(id)
        {
            FullName = fullName;
            DateOfBirth = dateOfBirth;
            Address = address;
            _telephoneNumbers = telephoneNumbers.ToList();
        }

        private Contact()
        {
        }

        public void UpdateName(FullName name)
            => FullName = name;

        public void UpdateDateOfBirth(DateOfBirth dateOfBirth)
            => DateOfBirth = dateOfBirth;

        public void UpdateAddress(Address address)
            => Address = address;

        public void UpdateTelephoneNumbers(IEnumerable<TelephoneNumber> numbers)
             => _telephoneNumbers = numbers.ToList();
    }
}