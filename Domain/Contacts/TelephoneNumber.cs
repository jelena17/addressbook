﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Domain
{
    public record TelephoneNumber : ValueObject<TelephoneNumber>
    {
        private Regex[] _namePatterns = new[] {
            new Regex(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$"),
            new Regex(@"^(\+\d{1,2}[\s.]?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{3}$"),
            new Regex(@"^(\+\d{5}\s)?[\s.-]?\d{3}[\s.-]?\d{3}$"),
            new Regex(@"^(\+\d{3}\s)?[\s.-]?\d{2}[\s.-]?\d{3}[\s.-]?\d{3}$"),
        };
        public string Value { get; }

        public TelephoneNumber(string value)
        {
            Validate(value);
            Value = value;
        }
        private void Validate(string value)
        {
            if (!_namePatterns.Any(pattern => pattern.IsMatch(value ?? "")))
                throw new ArgumentException($"Invalid value for {nameof(TelephoneNumber)}.");
        }
    }
}