﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Domain
{
    public record Zip : ValueObject<Zip>
    {
        private Regex[] _namePatterns = new[] {
            new Regex(@"^[0-9]{5}$"),
            new Regex(@"^[0-9]{5}-[0-9]{4}$"),
            new Regex(@"^[0-9]{5}\s?[0-9]{4}$"),
            new Regex(@"^[0-9]{6}$"),
        };
        public string Value { get; set; }

        public Zip(string value)
        {
            Validate(value);
            Value = value;
        }
        private void Validate(string value)
        {
            if (!_namePatterns.Any(pattern => pattern.IsMatch(value ?? "")))
                throw new ArgumentException($"Invalid value for {nameof(Zip)}.");
        }
    }
}