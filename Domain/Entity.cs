﻿namespace Domain
{
    public abstract class Entity
    {
        public int Id { get; private set; }

        protected Entity(int id)
        {
            Id = id;
        }

        protected Entity()
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is Entity otherEntity)
                return Id == otherEntity.Id;

            return false;
        }

        public override int GetHashCode()
            => Id.GetHashCode();
    }
}