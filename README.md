# README #

Address Book API documentation can be found on swagger.
To fully test, build project, run migrations and use swagger endpoints to create, delete, update and get contacts.

### Arhitecture ###
This API could be a very simple CRUD API, but for this test, I decided to apply DDD approach with CQRS.

* Web - Controller and network infrastructure (configuration of SignalR).
* App - Definition and implementation of commands and queries. 
* Domain - Model Contact entity and value objects (its attributes).
* Data Access - Implementation of repositories.
* Tests - Unit: Domain logic validation. Integration: Repository tests.
* Client - Application used to test SignalR (real-time update).