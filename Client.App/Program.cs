﻿using App.Contracts;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Client.App
{
    internal class Program
    {
        private const string URL = "https://localhost:44360/contacthub";

        private static async Task Main(string[] args)
        {
            HubConnection connection = SetUpConnection();
            SetUpListener(connection);

            await connection.StartAsync();

            Console.WriteLine("To stop listening press any key.");
            Console.ReadKey();
        }

        private static HubConnection SetUpConnection()
        {
            return new HubConnectionBuilder()
                .WithUrl(URL)
                .Build();
        }

        private static void SetUpListener(HubConnection connection)
        {
            connection.On<UpdateContactRequest>("ContactUpdate", (contact) =>
            {
                var simpleMessage = JsonSerializer.Serialize(contact,
                    new JsonSerializerOptions
                    {
                        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
                            & JsonIgnoreCondition.WhenWritingDefault
                    });
                Console.WriteLine($"Contact updated with info {simpleMessage}");
            });
        }
    }
}