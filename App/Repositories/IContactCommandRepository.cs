﻿using Domain;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IContactCommandRepository
    {
        Task Save(Contact contact);

        Task Delete(int contactId);
    }
}