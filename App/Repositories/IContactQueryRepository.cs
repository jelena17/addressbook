﻿using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Repositories
{
    public interface IContactQueryRepository
    {
        Task<Contact> GetById(int contactId);

        Task<IEnumerable<Contact>> Get(int pageSize, int pageNumber);
    }
}