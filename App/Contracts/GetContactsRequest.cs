﻿namespace App.Contracts
{
    public class GetContactsRequest
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}