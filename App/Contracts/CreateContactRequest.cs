﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Contracts
{
    public class CreateContactRequest
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public AdressDto Address { get; set; }

        [Required]
        public string[] TelephoneNumbers { get; set; }
    }
}