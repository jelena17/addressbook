﻿using System;

namespace App.Contracts
{
    public class UpdateContactRequest
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public AdressDto Address { get; set; }
        public string[] TelephoneNumbers { get; set; }
    }
}