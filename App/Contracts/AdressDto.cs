﻿using System.ComponentModel.DataAnnotations;

namespace App.Contracts
{
    public class AdressDto
    {
        [Required]
        public string AddressLine { get; set; }

        [Required]
        public string City { get; set; }

        public string State { get; set; }

        [Required]
        public string Zip { get; set; }

        [Required]
        public string Country { get; set; }
    }
}