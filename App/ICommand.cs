﻿using System.Threading.Tasks;

namespace App
{
    public interface ICommand<TRequest>
    {
        Task Execute(TRequest request);
    }
}