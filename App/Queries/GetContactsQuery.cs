﻿using App.Contracts;
using App.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Queries
{
    public class GetContactsQuery
        : IQuery<GetContactsRequest, IEnumerable<GetContactResponse>>
    {
        private readonly IContactQueryRepository _repo;

        public GetContactsQuery(IContactQueryRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<GetContactResponse>> Execute(GetContactsRequest request)
        {
            var contacts = await _repo.Get(request.PageSize, request.PageNumber);
            return contacts.Select(contact => contact.MapToDto());
        }
    }
}