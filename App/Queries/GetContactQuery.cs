﻿using App.Contracts;
using App.Repositories;
using System.Threading.Tasks;

namespace App.Queries
{
    public class GetContactQuery
        : IQuery<int, GetContactResponse>
    {
        private readonly IContactQueryRepository _repo;

        public GetContactQuery(IContactQueryRepository repo)
        {
            _repo = repo;
        }

        public async Task<GetContactResponse> Execute(int request)
        {
            var contact = await _repo.GetById(request);
            return contact.MapToDto();
        }
    }
}