﻿using App.Contracts;
using Domain;
using System.Linq;

namespace App.Queries
{
    public static class ContactExtensions
    {
        public static GetContactResponse MapToDto(this Contact contact)
        {
            return new GetContactResponse
            {
                Id = contact.Id,
                FullName = contact.FullName.Value,
                DateOfBirth = contact.DateOfBirth.Value,
                Address = new AdressDto
                {
                    AddressLine = contact.Address.AddresLine,
                    City = contact.Address.City,
                    State = contact.Address.State,
                    Zip = contact.Address.Zip.Value,
                    Country = contact.Address.Country.Value,
                },
                TelephoneNumbers = contact.TelephoneNumbers
                    .Select(telephoneNumber => telephoneNumber.Value)
                    .ToArray()
            };
        }
    }
}