﻿using System.Threading.Tasks;

namespace App
{
    public interface IQuery<TRequest, TResult>
    {
        Task<TResult> Execute(TRequest request);
    }
}