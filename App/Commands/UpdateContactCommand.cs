﻿using App.Contracts;
using App.Repositories;
using Domain;
using System.Linq;
using System.Threading.Tasks;

namespace App.Commands
{
    public class UpdateContactCommand
        : ICommand<UpdateContactRequest>
    {
        private readonly IContactQueryRepository _queryRepo;
        private readonly IContactCommandRepository _commandRepo;

        public UpdateContactCommand(
            IContactQueryRepository queryRepo,
            IContactCommandRepository commandRepo)
        {
            _queryRepo = queryRepo;
            _commandRepo = commandRepo;
        }

        public async Task Execute(UpdateContactRequest request)
        {
            var contact = await _queryRepo.GetById(request.Id);
            UpdateContact(contact, request);
            await _commandRepo.Save(contact);
        }

        private void UpdateContact(Contact contact, UpdateContactRequest request)
        {
            if (request.FullName != default)
                contact.UpdateName(new FullName(request.FullName));

            if (request.DateOfBirth != default)
                contact.UpdateDateOfBirth(new DateOfBirth(request.DateOfBirth));

            if (request.Address != default)
            {
                var address = new Address(
                  request.Address.AddressLine,
                  request.Address.City,
                  request.Address.State,
                  request.Address.Zip,
                  request.Address.Country);
                contact.UpdateAddress(address);
            }

            if (request.TelephoneNumbers != default)
            {
                var numbers = request.TelephoneNumbers
                    .Select(num => new TelephoneNumber(num));
                contact.UpdateTelephoneNumbers(numbers);
            }
        }
    }
}