﻿using App.Contracts;
using App.Repositories;
using Domain;
using System.Linq;
using System.Threading.Tasks;

namespace App.Commands
{
    public class CreateContactCommand
        : ICommand<CreateContactRequest>
    {
        private readonly IContactCommandRepository _repo;

        public CreateContactCommand(IContactCommandRepository repo)
        {
            _repo = repo;
        }

        public Task Execute(CreateContactRequest request)
        {
            Contact contact = CreateContact(request);
            return _repo.Save(contact);
        }

        private static Contact CreateContact(CreateContactRequest request)
        {
            var name = new FullName(request.FullName);
            var dateOfBirth = new DateOfBirth(request.DateOfBirth);

            var address = new Address(
                request.Address.AddressLine,
                request.Address.City,
                request.Address.State,
                request.Address.Zip,
                request.Address.Country);

            var numbers = request.TelephoneNumbers
                .Select(num => new TelephoneNumber(num));

            return new Contact(name, dateOfBirth, address, numbers);
        }
    }
}