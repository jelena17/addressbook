﻿using App.Repositories;
using System.Threading.Tasks;

namespace App.Commands
{
    public class DeleteContactCommand
        : ICommand<int>
    {
        private readonly IContactCommandRepository _repo;

        public DeleteContactCommand(IContactCommandRepository repo)
        {
            _repo = repo;
        }

        public Task Execute(int request)
            => _repo.Delete(request);
    }
}